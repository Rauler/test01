*** Settings ***
Library     SeleniumLibrary
Resource    resources.robot

*** Test Cases ***
01 Ir al blog de winston castillo
    Open Homepage
    Title Should Be         Hola Mundo!
    Set Focus To Element        xpath=/html/body/div[1]/div/div[2]/a[1]
    Click Link           xpath=/html/body/div[1]/div/div[2]/a[1]
    Wait Until Element Is Visible       xpath=//*[@id="post-437"]/div[1]/a
    Title Should Be         Winston Castillo – Un sitio para comunicarse
    Close Browser

02 Abrir ventana Modal
    [Tags]      0Dos
    Open Homepage
    Title Should Be         Hola Mundo!
    Click Element           xpath=/html/body/ul/li[1]
    Set Focus To Element        xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link           xpath=/html/body/div[1]/div/div[2]/a[2]
    Title Should Be         Hola Mundo!
    Wait Until Element Is Visible       xpath=//*[@id="exampleModal"]/div/div/div[1]
    Close Browser